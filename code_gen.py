
call = 0

def write_headers(f):
	f.write("from confluent_kafka import Producer \nfrom confluent_kafka import Consumer, KafkaError \n")
	f.write("\nsettings = { 'bootstrap.servers': 'localhost:9092',\n 'group.id': 'mygroup', \n 'client.id': 'client-1', \n 'enable.auto.commit': True, \n 'session.timeout.ms': 6000, \n 'default.topic.config': {'auto.offset.reset': 'smallest'} }")
	f.write("\n\np = Producer({'bootstrap.servers': 'localhost:9092'})")


def write_producer(f):
	f.write("\n\ndef acked(err, msg):\n")
	f.write("\tif err is not None:\n")
	f.write("\t\tprint(\"Failed to deliver message: {0}: {1}\".format(msg.value(), err.str())) \n\n")

	f.write("def producer(val):\n")
	f.write("\ttry:\n")
	f.write("\t\tp.produce('From_Client', val, callback=acked)\n")
	f.write("\t\tp.poll(0.5)\n")

	f.write("\n\texcept KeyboardInterrupt:\n")
	f.write("\t\tpass\n")

	f.write("\n\tp.flush(30)\n\n")

	f.write("\nc = Consumer(settings)\n")
	f.write("c.subscribe(['From_Server'])\n\n\n")

	f.write("def Get_Result(query, server_data):\n")
	f.write("\tproducer(server_data)\n")
	f.write("\tresult = \"\" \n")
	f.write("\twhile 1:\n")
	f.write("\t\tmsg = c.poll(0.1)\n")
	f.write("\t\tif msg is None:\n")
	f.write("\t\t\tcontinue\n")
	f.write("\t\telif not msg.error():\n")
	f.write("\t\t\tresult = msg.value().decode(\"utf-8\")\n")
	f.write("\t\t\tbreak\n")
	f.write("\tprint(\"\\nResult of \",query,\" is \",result) \n")
	# f.write("\tprint(\"\\n\") \n\n")




def acked(err, msg):
    if err is not None:
        print("Failed to deliver message: {0}: {1}"
              .format(msg.value(), err.str()))
    # else:
    #     print("Message produced: {0}".format(msg.value()))



def producer(val):
    try:
        p.produce('From_Client', val, callback=acked)
        p.poll(0.5)

    except KeyboardInterrupt:
        pass

    p.flush(30)




def process_funcn(data, gen_file):

	global call
	funcn_name = data.split("(")[0]

	if call == 0:
		write_headers(gen_file)
		write_producer(gen_file)
		call += 1

	char1 = '('
	char2 = ')'
	params = data[data.find(char1)+1 : data.find(char2)]

	params_list = params.split(",")
	params_count = len(params_list)
	params_server = ""
	params_type = ""

	print("\nEnter "+str(params_count)+" value")

	for i in range(0, params_count):
		x = input("Enter Value : ")
		params_server += str(x)

		if x.isalpha():
			params_type += "str"

		elif x.isdigit():
			params_type += "int"
		
		else:
			params_type += "float"

		
		if i != params_count-1:
			params_server += "&"
			params_type += "^"

	
	gen_file.write("\nserver_data = " + "\"" + funcn_name+"\"" + "+\"" + "$" + "\"+" + "\"" + params_server + "\"+"+ "\"" + "$" + "\"+" + "\"" + params_type + "\"")
	# server_data = funcn_name + "$" + params_server + "$" + params_type
	gen_file.write("\nGet_Result(" + "\""+data+"\",server_data)")
	


def processing_file():
	with open('input.txt', 'r') as file:
		data = ""
		data = file.read().replace('\n', ' ')

	gen_file = open("rpc_call"+".py","w")
	
	data = data.split(" ")
	data.pop()
	print(data)
	for val in data:
		process_funcn(val, gen_file)

processing_file()