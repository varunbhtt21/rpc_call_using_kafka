from confluent_kafka import Producer 
from confluent_kafka import Consumer, KafkaError 

settings = { 'bootstrap.servers': 'localhost:9092',
 'group.id': 'mygroup', 
 'client.id': 'client-1', 
 'enable.auto.commit': True, 
 'session.timeout.ms': 6000, 
 'default.topic.config': {'auto.offset.reset': 'smallest'} }

p = Producer({'bootstrap.servers': 'localhost:9092'})

def acked(err, msg):
	if err is not None:
		print("Failed to deliver message: {0}: {1}".format(msg.value(), err.str())) 

def producer(val):
	try:
		p.produce('From_Client', val, callback=acked)
		p.poll(0.5)

	except KeyboardInterrupt:
		pass

	p.flush(30)


c = Consumer(settings)
c.subscribe(['From_Server'])


def Get_Result(query, server_data):
	producer(server_data)
	result = "" 
	while 1:
		msg = c.poll(0.1)
		if msg is None:
			continue
		elif not msg.error():
			result = msg.value().decode("utf-8")
			break
	print("\nResult of ",query," is ",result) 

server_data = "sum_func"+"$"+"5&2"+"$"+"int^int"
Get_Result("sum_func(x,y)",server_data)
server_data = "mul_func"+"$"+"8&4"+"$"+"int^int"
Get_Result("mul_func(x,y)",server_data)