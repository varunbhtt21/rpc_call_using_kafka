from Library_Function import*
import base64
from confluent_kafka import Producer
from confluent_kafka import Consumer, KafkaError
import threading 

settings = {
    'bootstrap.servers': 'localhost:9092',
    'group.id': 'mygroup',
    'client.id': 'client-1',
    'enable.auto.commit': True,
    'session.timeout.ms': 6000,
    'default.topic.config': {'auto.offset.reset': 'smallest'}
}

p = Producer({'bootstrap.servers': 'localhost:9092'})


def acked(err, msg):
    if err is not None:
        print("Failed to deliver message: {0}: {1}"
              .format(msg.value(), err.str()))
    else:
        print("Message produced: {0}".format(msg.value()))



def producer(val):
    try:
        print("From_Server",val)
        p.produce('From_Server', val, callback=acked)
        p.poll(0.5)

    except KeyboardInterrupt:
        pass

    p.flush(30)





def process_funcn(funcn_name, params, flag):

	try:
		value = globals()[funcn_name](params, flag)

	except:
		value = "ERROR : "+funcn_name+"() does not exist"
	
	return str(value) 




def processing(request):

	funcn_name = request.split("$")[0]
	params = request.split("$")[1].split("&")
	params_type = request.split("$")[2].split("^")
	
	transformed_params = []
	flag = 0

	for val in range(0, len(params_type)):
		if params_type[val] == "int":
			transformed_params.append(int(params[val]))

		if params_type[val] == "float":
			transformed_params.append(float(params[val]))

		if params_type[val] == "str":
			flag = 1
			transformed_params.append(str(params[val]))


	result = process_funcn(funcn_name, transformed_params, flag)

	return result




c = Consumer(settings)
c.subscribe(['From_Client'])


def Run_Server():
	while True: 
		request = ""
		while 1:
			msg = c.poll(0.1)
			if msg is None:
				continue
			elif not msg.error():
				request = msg.value().decode("utf-8")
				break  

		encoded = processing(request)
		producer(encoded)



t1 = threading.Thread(target=Run_Server) 
t1.start() 
  


