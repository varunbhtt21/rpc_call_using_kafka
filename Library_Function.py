

def sum_func(params, flag):

	try:
		if flag == 0:
			value = 0
			for val in params:
				value += val

		else:
			value = ""
			for val in params:
				value += str(val)

	except:
		value = "Error : Can't Add"

	return value


def mul_func(params, flag):

	try:
		if flag == 0:
			value = 1
			for val in params:
				value = int(value) * val

		else:
			value = ""
			for val in params:
				value = value * val

	except:
		value = "Error : Strings Can't Multiply"

	return value

